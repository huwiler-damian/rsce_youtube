# Youtube Custom Element #
Dieses Custom Element bietet diverse Einstellungen bez�glich der Youtube-Video-Einbindung:

* Video-ID
* Startzeit/Endzeit des Videos
* Auto-Play
* Bedienung einblenden
* Vollbild aktivieren
* Youtube-Logo einblenden
* Weitere Vorschl�ge
* Loop
* Informationen einblenden (Videotitel, Name des Uploaders,...)
* Wiedergabequalit�t
* Hintergrundbild (sichtbar w�rend Video noch nicht geladen ist)

Damit das Custom Element unterschiedlich verwendet werden kann, gibt es drei Arten der Einbindung:

* Das Element nimmt die volle Bildschirmh�he- und Breite an (geeignet f�r Slider)
* Das Element hat eine vorgegebene Breite und die H�he wird automatisch anhand des ausgew�hlten Seitenverh�ltniss berechnet
* Das Element hat immer 100% Breite des umfassenden Wrappers

Einbindung
* rsce_youtube.html5 und rsce_youtube_config.php m�ssen in den Template-Ordner hochgeladen werden
* Der Inhalt der Datei script.js sollte in eine Javascript-Datei eingesetzt werden
