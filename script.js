'use strict';

jQuery(function($){
	$(document).ready(function(){
		
		// !Youtube Video
		// Youtube Video
		// 2. This code loads the IFrame Player API code asynchronously.
		var tag = document.createElement('script');
	
		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
		
	});
});

// Iniitalise CustomElement of Youtube
// Function has to be global
function onYouTubeIframeAPIReady() {
	$(".youtube-player > div").each(function(){
		var playerOptions = $(this).data("ytParameter");
		
		playerOptions.events = {};
		playerOptions.events.onReady = onPlayerReady;
		
		var player = new YT.Player($(this).attr('id'), playerOptions );
	});
}
		
function onPlayerReady(objPlayer){
	if ( $( objPlayer.target.a ).data("playbackquality") !== 'auto' ) {
		objPlayer.target.setPlaybackQuality($( objPlayer.target.a ).data("playbackquality"));
	}
}
