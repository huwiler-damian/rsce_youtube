<?php

return array(
	'label' => array(
		'de' => array(
			'Youtube-Video',
			'Benutzerdefinierter Youtube-Player',
		),
	),
	'types' => array('content'),
	'contentCategory' => 'MEMO Customs',
	'fields' => array(
		'video_id' => array(
			'label' => array(
				'de' => array('Video-ID', 'Die Video-ID des Youtube-Videos'),
			),
			'inputType' => 'text',
			'eval' => array(
				'tl_class' => 'w100',
				'mandatory' => 'true',
			),
		),
		'player_start' => array(
			'label' => array(
				'de' => array('Startzeit des Videos', 'Die Startzeit des Videos in Sekunden'),
			),
			'inputType' => 'text',
			'eval' => array(
				'tl_class' => 'w50',
			),
		),
		'player_end' => array(
			'label' => array(
				'de' => array('Endzeit des Videos', 'Die Endzeit des Videos in Sekunden'),
			),
			'inputType' => 'text',
			'eval' => array(
				'tl_class' => 'w50',
			),
		),
		'play_auto' => array(
			'label' => array(
				'de' => array('Auto-Play', 'Soll der Video automatisch nach dem laden gestartet werden?'),
			),
			'inputType' => 'checkbox',
			'eval' => array(
				'tl_class' => 'w50',
			),
		),
		'control' => array(
			'label' => array(
				'de' => array('Bedienung einblenden', 'Soll die Bedienung eingeblendet werden?'),
			),
			'default' => true,
			'inputType' => 'checkbox',
			'eval' => array(
				'tl_class' => 'w50',
			),
		),
		'fullwidth' => array(
			'label' => array(
				'de' => array('Vollbild aktivieren', 'Falls dieses Feld ausgewählt ist, wird die Schaltfläche für den Vollbild-Modus eingeblendet (funktioniert nur wenn die Bedienung eingeblendet ist).'),
			),
			'default' => true,
			'inputType' => 'checkbox',
			'eval' => array(
				'tl_class' => 'w50',
			),
		),
		'modestbranding' => array(
			'label' => array(
				'de' => array('Youtube-Logo einblenden', 'Soll das Youtube-Logo eingeblendet werden (kann nur ausgeblendet werden wenn Bedienung aktiviert ist)?'),
			),
			'inputType' => 'checkbox',
			'eval' => array(
				'tl_class' => 'w50',
			),
		),
		'rel' => array(
			'label' => array(
				'de' => array('Weitere Vorschläge', 'Es werden am Ende des Videos weitere Vorschläge aufgelistet'),
			),
			'inputType' => 'checkbox',
			'eval' => array(
				'tl_class' => 'w50',
			),
		),
		'loop' => array(
			'label' => array(
				'de' => array('Loop', 'Eine unendliche Schleife des Videos'),
			),
			'inputType' => 'checkbox',
			'eval' => array(
				'tl_class' => 'w50',
			),
		),
		'showinfo' => array(
			'label' => array(
				'de' => array('Informationen einblenden (Videotitel, Name des Uploaders, ...)', 'Informationen, wei den Videotitel oder den Namen des Uploaders einblenden'),
			),
			'inputType' => 'checkbox',
			'eval' => array(
				'tl_class' => 'w50',
			),
		),
		'bg_image' => array(
			'label' => array(
				'de' => array('Hintergrund-Bild', 'Ist wärend dem Laden des Videos sichtbar'),
			),
			'inputType' => 'fileTree',
			'eval' => array(
				'tl_class' => 'clr',
				'filedType' => 'checkbox',
				'filesOnly' => true,
			),
		),
		'quality' => array(
			'label' => array(
				'de' => array('Wiedergabe-Qualität', 'Die Qualität des Videos'),
			),
			'inputType' => 'select',
			'eval' => array(
				'tl_class' => 'clr',
				'mandatory' => true,
			),
			'options' => array(
				'auto' => 'auto',
				'highres' => 'highres',
				'hd1080' => 'hd1080',
				'hd720' => 'hd720',
				'large' => 'large',
				'medium' => 'medium',
				'small' => 'small',
			),
		),
        'description-full' => array(
            'label' => array('Art der Einsetzung', ''),
            'inputType' => 'group',
        ),
		'art' => array(
			'label' => array(
				'de' => array('Art der Einsetzung', 'Je nach Auswahl, müssen noch einige Einstellungen gemacht werden'),
			),
			'inputType' => 'radio',
			'eval' => array(
				'tl_class' => 'w100',
				'mandatory' => true
			),
			'options' => array(
				'full' => 'Volle Bildschrimgrösse und Höhe',
				'width' => 'Vorgegebene Playerbreite',
				'wrapper' => 'Breite an umfassenden Wrapper anpassen',
			),
		),
		'format' => array(
			'label' => array(
				'de' => array('Seitenverhältniss des Videos', ''),
			),
			'inputType' => 'select',
			'eval' => array(
				'tl_class' => 'w100',
			),
			'options' => array(
				'1920,1080' => '1920 x 1080',
				'1840,971' => '1840 x 971',
				'1280,800' => '1280 x 800',
				'1080,720' => '1080 x 720',
				'16,9' => '16 x 9',
			),
		),
		'margin' => array(
			'label' => array(
				'de' => array('Abgeschnittener Teil', '(wird nur für "Volle Bildschrimgrösse und Höhe" benötigt)'),
			),
			'inputType' => 'text',
			'eval' => array(
				'tl_class' => 'w50',
			),
		),
		'player_width' => array(
			'label' => array(
				'de' => array('Breite des Players', '(wird nur für "Breite an umfassenden Wrapper anpassen" benötigt)'),
			),
			'inputType' => 'text',
			'eval' => array(
				'tl_class' => 'w50',
			),
		),
	),
);
